<?php
require_once '../src/Virgo/Labs/ColorArt/ColorArt.php';
use Virgo\Labs\ColorArt\ColorArt;

$images = array("test1.png", "test2.png","test3.jpg","test4.jpg","test5.jpg");

foreach ($images as $img) {
	$ca = new ColorArt($img);


	//Merge colors to the top <mergeColorsTopCount> colors, where  r,g,b  differences are in mergeColorsThreshold
	// default: true, 100, 10
	$ca -> setMergeColors(true);
	$ca -> setMergeColorsThreshold(15);

	//Round colors: round down the rgb's last digits to 5 or 0
	// default: true
	$ca -> setRoundColors(true);

	//Scale image to specified width and height
	//default: 320x320
	$ca -> setScaleTo(350);

	$ca -> setEdgeMode(ColorArt::BOTTOM_EDGE);

	$ca->analyze();

	var_dump($ca->getInfo());

?>
<div style="overflow:auto;background-color:rgb(<?=$ca->getBackgroundColor()->toString()?>)">
<img src="<?=$img?>" style="max-width:200px;max-height: 200px;float: left;"  />
<h1 style="color:rgb(<?=$ca->getPrimaryColor()->toString()?>)">Primary Color (<?=$ca->getPrimaryColor()->isContrasting($ca->getBackgroundColor())?>)</h1>
<h2 style="color:rgb(<?=$ca->getSecondaryColor()->toString()?>)">Secondary Color</h1>
<h3 style="color:rgb(<?=$ca->getDetailColor()->toString()?>)">Detail Color</h3>
</div>

<?php
}
?>