<?php
namespace Virgo\Labs\ColorArt;
require_once dirname(__FILE__)."/Color.php";

use Virgo\Labs\ColorArt\RGB;

/**
 * Class ColorArt
 */
class ColorArt {

    const  LEFT_EDGE    = 0;
    const  RIGHT_EDGE   = 1;
    const  TOP_EDGE     = 2;
    const  BOTTOM_EDGE  = 3;

	public $image;
	
	private $size;
	private $width;
	private $height;
	private $resizedImage;
	private $originalImage;
	private $imageColors = array();
    private $edgeColors;

	private $backgroundColor;
	private $primaryColor;
	private $secondaryColor;
	private $detailColor;
	private $darkBackground;

    private $mergeColors = true;
    private $mergeColorsThreshold = 10;
    private $roundColors = true;

    private $edgeMode = ColorArt::LEFT_EDGE;

    private $info = array();

    public $scaleTo = 320;

	/**
    * Constructor 
    *
    * @param string $imagePath Image path on the filesystem 
    * @throws InvalidArgumentException Image not found 
    * @return ColorArt Instance of ColorArt
    */	
    public function __construct( $imagePath ) {
        if ( empty($imagePath) || !is_file($imagePath) ) {
            throw new InvalidArgumentException("Image not found: ".$imagePath);
        }
        $this->image = $imagePath;
    }

	
    /**
     * Analyze colors of the image
     * @throws InvalidArgumentException Image is not valid or cannot be analyzed
     * @throws RuntimeException
     */
    public function analyze() {
    	//load image

        try {
            
            $start_time = time();

            $this->scaleDownImage();
            $this->backgroundColor = $this->findEdgeColor();
            if ($this->mergeColors) {
                $merge_time = time();
                $this->doMergeColors($this->imageColors);
                $this->bm("image_colors_merge_runtime", $merge_time);
                $this->log("image_colors_count_merged", count($this->imageColors));
            }
            $this->darkBackground = $this->backgroundColor->isDark();
            $this->findTextColors();

            if (!$this->primaryColor) {
                if ($this->darkBackground) {
                    $this->primaryColor = new RGB(255,255,255);
                } else {
                    $this->primaryColor = new RGB(0,0,0);
                }
            }

            if (!$this->secondaryColor) {
                if ($this->darkBackground) {
                    $this->secondaryColor = new RGB(255,255,255);
                } else {
                    $this->secondaryColor = new RGB(0,0,0);
                }
            }

            if (!$this->detailColor) {
                if ($this->darkBackground) {
                    $this->detailColor = new RGB(255,255,255);
                } else {
                    $this->detailColor = new RGB(0,0,0);
                }
            }

            $this->bm("full_runtime_sec", $start_time);
        } catch (Exception $e) {
            throw $e;
        }
    	

    }

    /**
     * Find the primary, secondary and detail color in the colors of image
     */
    private function findTextColors() {
        
        //imagecolors is already sorted
        $this->imageColors=array_reverse($this->imageColors,true);
        $findDarkTextColor = !$this->darkBackground;
        $sortedColors = array();

        foreach ($this->imageColors as $cstr => $count) {
        	if (strlen($cstr)<5) {
        		continue;
        	}
        	
        	$color = RGB::fromString($cstr);
        	//$color->minimumSaturation(0.15);
        	
        	
        	if ($color->isDark() == $findDarkTextColor) {
        		$sortedColors[$color->toString()] += $count;
        	}
        	
        }	
        natsort($sortedColors);
        $sortedColors = array_reverse($sortedColors);


        foreach ($sortedColors as $cstr => $count) {
        	if (strlen($cstr)<5) {
        		continue;
        	}
        	

        	$color = RGB::fromString($cstr);
        	if (!$this->primaryColor) {
        		if ($color->isContrasting($this->backgroundColor)) {
        			$this->primaryColor = $color;
        		}
        	} 
        	else if (!$this->secondaryColor) {
        		if ( 
        			!$this->primaryColor->isDistinct($color) || 
        			!$color->isContrasting($this->backgroundColor)
        		) 
        		{
        			continue;
       			}
    			$this->secondaryColor = $color;
        	}
        	else if (!$this->detailColor) {
        		if ( 
        			!$this->primaryColor->isDistinct($color) || 
        			!$this->secondaryColor->isDistinct($color)|| 
        			!$color->isContrasting($this->backgroundColor)
        		) 
        		{
        			continue;
       			}
    			$this->detailColor = $color; 
    			break; 
        	}
        }
    }

    /**
     * Merge the similar colors of the image
     * @param  array $imageColors
     */
    private function doMergeColors(array &$imageColors) {
        //sort order by count desc
        natsort($imageColors);
        array_reverse($imageColors,true);
        
        //group colors
        // group is threshold 

        $groups = array();

        foreach ($imageColors as $color => $count) {
            //create group key 
            // 233 77 99   
            // 239 68 100
            $colorParts = explode(",", $color);
            if (count($colorParts) != 3) {
                continue;
            }
            $groupParts = array();
            for ( $i=0; $i<3;$i++ ) {
                $groupParts[$i] = floor($colorParts[$i] / $this->mergeColorsThreshold) * $this->mergeColorsThreshold;
            }
            $groupKey = implode(",", $groupParts);

            $groupInfo = $groups[$groupKey];
            if (empty($groupInfo)) {
                $groupInfo['color'] = $color;
                $groupInfo['count'] = $count;
            } else {
                $groupInfo['count'] += $count;
            }
            $groups[$groupKey] = $groupInfo;

            $imageColors[$color] = 0;

        }
        
        foreach ($groups as $groupInfo) {
            $color = $groupInfo['color'];
            $count = $groupInfo['count'];
            $imageColors[$color] = $count;
        }

        $callback = 
                function ($var) {
                    return (!($var==0));
                };
        $imageColors = array_filter($imageColors, $callback);
        natsort($imageColors);
    }

    /**
     * Find the background color of the image
     */
    private function findEdgeColor() {

    	$edgeColors = array();
        $start_time = time();
    	for ($x=0; $x<$this->width;$x++) {
    		for ($y=0; $y<$this->height;$y++) {
    			$index = imagecolorat($this->resizedImage, $x, $y);
                $Colors = imagecolorsforindex($this->resizedImage, $index);

                //Round colors
                if ($this->roundColors) {
                    $Colors['red'] = $Colors['red'] - ($Colors['red'] % 5);
                    $Colors['green'] = $Colors['green'] - ($Colors['green'] % 5);
                    $Colors['blue'] = $Colors['blue'] - ($Colors['blue'] % 5);
                }
               	$color = new RGB($Colors['red'], $Colors['green'], $Colors['blue']);
                if (
                    (
                        ( $this->edgeMode == ColorArt::LEFT_EDGE ) &&
                        ( $x == 0 )
                    ) || 
                    (
                        ( $this->edgeMode == ColorArt::RIGHT_EDGE ) &&
                        ( $x == $this->width - 1 )
                    ) ||
                    (
                        ( $this->edgeMode == ColorArt::TOP_EDGE ) &&
                        ( $y == 0 )
                    ) ||
                    (
                        ( $this->edgeMode == ColorArt::BOTTOM_EDGE ) &&
                        ( $y == $this->height - 1 )
                    )
                    
                ) 
                {
                	$edgeColors[] = $color->toString();//$hexcolor;
                }
                $this->imageColors[] =  $color->toString(); //$hexcolor;

    		}
    	}
        $this->bm("color_extract_runtime_sec", $start_time);

        //drop image 
        unset($this->resizedImage);

        //preset imagecolors
        $this->imageColors = array_count_values($this->imageColors);
        $this->log("image_colors_count", count($this->imageColors));

        natsort($this->imageColors);
        
    	//remove colors with low count
    	$edgeColors=array_count_values($edgeColors);
        $this->log("edge_colors_count", count($edgeColors));
    	natsort($edgeColors);
        $edgeColors=array_reverse($edgeColors,true);

        if ($edgeColors[0]>2) {
            $callback = 
                function ($var) {
                    return (!($var<3));
                };
            $sortedColors = array_filter($edgeColors,$callback);
        } else {
       	    $sortedColors = $edgeColors;
        }
        $this->log("edge_colors_count_sorted", count($sortedColors));
        if ($this->mergeColors) {
            $start_time = time();
            $this->doMergeColors($sortedColors);
            $this->bm("edge_colors_merge_runtime_sec", $start_time);
            $this->log("edge_colors_count_merged", count($sortedColors));
            $sortedColors = array_reverse($sortedColors, true);
        }
        $proposedColor;
        $this->edgeColors = $sortedColors;
        if ( count($sortedColors) >0 ) {
        	reset($sortedColors);
			
			$proposedColorCount = current($sortedColors);	
        	$proposedColor = RGB::fromString(key($sortedColors));
        	

        	if ($proposedColor->isBlackOrWhite()) {
        		//search for other color

        		for ($i=1; $i<count($sortedColors); $i++) {
        			$nextProposedColorCount = next($sortedColors);
        			$nextProposedColor = RGB::fromString(key($sortedColors));
        			

        			// make sure the second choice color is 30% as common as the first choice
        			if (($nextProposedColorCount / $proposedColorCount) > 0.3 ) {
						if ( $nextProposedColor->isBlackOrWhite) {
							$proposedEdgeColor = $nextProposedColor;
							$proposedColorCount = $nextProposedColorCount;
							break;
						}
					}
					else {
						// reached color threshold less than 30% of the original proposed edge color so bail
						break;
					}
        		}
        	}
        }

        return $proposedColor;

    }

    private function bm($name, $time) {
        $this->info[$name] = time() - $time;
    }

    private function log($name, $value) {
        $this->info[$name] = $value;
    }

    /**
     * Scale down the image 
     * @throws InvalidArgumentException Invalid image
     * @throws RuntimeException GD is not installed
     */
    private function scaleDownImage() {

        if (!function_exists('imagecreatetruecolor')) {
            throw new RuntimeException("GD (>2.0.1) is not installed");
        }

        $start_time = time();

    	$SCALE_WIDTH    = $this->scaleTo; 
        $SCALE_HEIGHT   = $this->scaleTo;
        $size = @GetImageSize($this->image);
        if (!$size) {
            throw new \InvalidArgumentException("Invalid image");
        }
        $scale=1;
        if ($size[0]>0)
        $scale = min($SCALE_WIDTH/$size[0], $SCALE_HEIGHT/$size[1]);
        if ($scale < 1)
        {
            $width = floor($scale*$size[0]);
            $height = floor($scale*$size[1]);
        }
        else
        {
            $width = $size[0];
            $height = $size[1];
        }
        $this->resizedImage = imagecreatetruecolor($width, $height);
        if ($size[2]==1)
        	$this->originalImage = imagecreatefromgif($this->image);
        if ($size[2]==2)
        	$this->originalImage = imagecreatefromjpeg($this->image);
        if ($size[2]==3)
        	$this->originalImage = imagecreatefrompng($this->image);

        imagecopyresampled($this->resizedImage, $this->originalImage, 0, 0, 0, 0, $width, $height, $size[0], $size[1]);
        $this->width = imagesx($this->resizedImage);
        $this->height = imagesy($this->resizedImage);

        $this->bm("scale_runtime_sec", $start_time);
    }

    /**
     * [getInfo description]
     * @return array
     */
    public function getInfo() {
        return $this->info;
    }


    /**
     * [getBackgroundColor description]
     * @return RGB
     */
    public function getBackgroundColor() {
        return $this->backgroundColor;
    }

    /**
     * [getPrimaryColor description]
     * @return RGB
     */
    public function getPrimaryColor() {
        return $this->primaryColor;
    }

    /**
     * [getSecondaryColor description]
     * @return RGB
     */
    public function getSecondaryColor() {
        return $this->secondaryColor;
    }

    /**
     * [getDetailColor description]
     * @return RGB
     */
    public function getDetailColor() {
        return $this->detailColor;
    }

    /**
     * [setMergeColors description]
     * @param boolean $mergeColors
     */
    public function setMergeColors($mergeColors = true) {
        if (is_bool($mergeColors)) {
            $this->mergeColors = $mergeColors;
        } else {
            throw new \InvalidArgumentException();
        }
        
    }  


    /**
     * [setMergeCOlorsThreshold description]
     * @param integer $threshold
     */
    public function setMergeCOlorsThreshold($threshold = 15) {
        if (is_int($threshold)) {
            $this->mergeColorsThreshold = $threshold;   
        } else {
            throw new \InvalidArgumentException();
        }
    }

    /**
     * [setRoundColors description]
     * @param boolean $roundColors
     */
    public function setRoundColors($roundColors = true) {
        if (is_bool($roundColors)) {
            $this->roundColors = $roundColors;   
        } else {
            throw new \InvalidArgumentException();
        }

    }

    /**
     * [setScaleTo description]
     * @param integer $scaleTo
     */
    public function setScaleTo($scaleTo = 320) {
        if (is_int($scaleTo)) {
            $this->scaleTo = 320;
        } else {
            throw new \InvalidArgumentException();
        }
    }

    public function setEdgeMode($mode) {
        if (
            $mode != ColorArt::LEFT_EDGE &&
            $mode != ColorArt::RIGHT_EDGE &&
            $mode != ColorArt::TOP_EDGE &&
            $mode != ColorArt::BOTTOM_EDGE
        )
            throw new \InvalidArgumentException();

        $this->edgeMode = $mode;
    }

}




?>



