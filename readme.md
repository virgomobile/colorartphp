##Introduction
Extract main colors from image.

Based on: [ColorArt](http://www.panic.com/blog/2012/12/itunes-11-and-colors/)

* * *

##Usage
	<?php
	try {

		$ca = new ColorArt($img);

		//merge similar colors - analyze will be slower
		$ca -> setMergeColors(true);
		//set the threshold of similarity 
		$ca -> setMergeColorsThreshold(15); 

		//Round colors: round down the last digits of rgb values to 5 or 0
		$ca -> setRoundColors(true);

		//Scale image to specified width and height, default is: 320x320px
		$ca -> setScaleTo(350);

		//choose an edge for background color's analyzing, default is: ColorArt::LEFT_EDGE
		$ca -> setEdgeMode(ColorArt::BOTTOM_EDGE);

		//Analyze picture
		$ca->analyze();

		//Get colors
		$bg = $ca->getBackgroundColor();
		$pc = $ca->getPrimaryColor();
		$sc = $ca->getSecondaryColor();
		$dc = $ca->getDetailColor();

	} catch (Exception $e) {

	}
	?>

	<!-- Get rgb color string -->
	<div style="backgorund-color:rgb(<?=$bg->toString()?>)">

	</div>

	<!-- Get hex value -->
	
	<div style="backgorund-color:#<?=$bg->toHex()->toString()?>">

	</div>

